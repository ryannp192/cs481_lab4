import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class ListItem {
  String image;
  String itemTitle;
  String desc;
  double price;

  ListItem(String image, String itemTitle, String desc, double price) {
    this.image = image;
    this.itemTitle = itemTitle;
    this.desc = desc;
    this.price = price;
  }

  Widget buildListItem(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      child: SizedBox(
        width: double.infinity,
        height: 375,
        child: Column(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(50.0),
            child: Image.asset(image, fit: BoxFit.fill,),
            ),
            Center(child: Text(itemTitle + " " + "\$$price")),
            Flexible(
              child: Text(desc),
            ),
            RaisedButton(child: Text("Add To Cart"), onPressed: () {}),
          ],
        ),
      ),
    );
  }
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final ourAppTitle = 'The CSUSM Student Store';
    const PrimaryColor = const Color(0xFF0051D8);
    return MaterialApp(
      title: ourAppTitle,
      theme: ThemeData(
        primaryColor: PrimaryColor,
      ),
      home: BuildHomePage(title: ourAppTitle),
      );

  }
}

class BuildHomePage extends StatelessWidget {
  final String title;
  final List<ListItem> items = [
    new ListItem("images/bean.jpg", "CSUSM Beanie",
        "California State University Knit Hat", 24.99),
    new ListItem("images/hat.jpg", "CSUSM Hat",
        "California State University Cap", 25.99),
    new ListItem("images/LongSleeve.jpg", "CSUSM Long Sleeve",
        "California State University Long Sleeve Shirt", 44.99),
    new ListItem("images/shortSleeve.jpg", "CSUSM Short Sleeve",
        "California State University Short Sleeve Shirt", 44.99),
    new ListItem("images/Orn.jpg", "CSUSM Ornament ball",
        "California State University Ornament", 11.95),
    new ListItem("images/Pullover.jpg", "CSUSM 1/4 zip Pullover",
        "California State University zip Pullover", 70.00),

  ];

  BuildHomePage({Key key, this.title}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFB0BEC5),
      appBar: AppBar(
        title: Text(title),
        actions: <Widget>[
          PopupMenuButton(
            itemBuilder: (context) => [
              PopupMenuItem(
                child: Text('Sign In'),
              ),
              PopupMenuItem(
                child: Text('Sign Up'),
              ),
              PopupMenuItem(
                child: Text('Subscribe'),
              ),
            ],
            tooltip: "Press for additional options",
          ),
        ],
      ),
      body: ListView.builder(
        itemCount: items.length,
        itemBuilder: (context, index) {
          return items[index].buildListItem(context);
        },
        scrollDirection: Axis.vertical,
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.blue,
                image: DecorationImage(
                  fit: BoxFit.fill,
                  image: AssetImage('images/csusmlogo.jpg'),
                ),
              ),
            ),
            ListTile(
              leading: Icon(Icons.home),
              title: Text('Homepage'),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            ListTile(
              leading: Icon(Icons.shopping_cart),
              title: Text('View Cart'),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            ListTile(
              leading: Icon(Icons.shopping_basket),
              title: Text('Checkout'),
              onTap: () {},
            ),
            ListTile(
              leading: Icon(Icons.settings),
              title: Text('Settings'),
              onTap: () {
                Navigator.pop(context);
              },
            ),
          ],
        ),
      ),
    );
  }
}
